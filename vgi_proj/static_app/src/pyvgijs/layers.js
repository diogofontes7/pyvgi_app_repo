//Set basemap view and Create tile Layers for the basemaps
var map = L.map('mapid').setView([lat, lon], 13);
var osm = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {maxZoom: 19,attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'}).addTo(map);
var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {maxZoom:19, attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'});

//Create the Cluster Groups to store the markers 
var fmarker = L.markerClusterGroup();
var tmarker = L.markerClusterGroup();
var allmarker = L.markerClusterGroup();
var osmmarker = L.markerClusterGroup();

//Create sensor marker icon
var sicon = L.icon({
            iconUrl: 'https://png.icons8.com/color/1600/sensor',
                    iconSize: [30,30],
                    iconAnchor: [12.5,30],
                    popupAnchor: [0,-30]
                    });

//Create flickr and twitter marker icons
var ficon = L.icon({
            iconUrl: 'https://www.arcgis.com/apps/PublicInformation/images/map/flickr25x30.png',
                    iconSize: [25,30],
                    iconAnchor: [12.5,30],
                    popupAnchor: [0,-30]
                    });

var ticon = L.icon({
            iconUrl: 'https://www.arcgis.com/apps/PublicInformation/images/map/twitter25x30.png',
                    iconSize: [25,30],
                    iconAnchor: [12.5,30],
                    popupAnchor: [0,-30]
                    });

//Create sensor marker
var sensormarker = L.marker([40.185606, -8.414708],{icon: sicon}).addTo(map);
sensormarker.bindPopup("<b style='color:DeepSkyBlue;'>Rain Sensor</b>"+"<br>"+"<b>Civil Engineering Department of University of Coimbra</b>")

//Add the basemaps and the data layers to the map
allmarker.addTo(map);

var baseMaps = {
            'OSM': osm,
            'Esri Sattelite': Esri_WorldImagery
        }

var groupedOverlays = {
            "Layers": {
                        "All": allmarker,
                                "Flickr": fmarker,
                                "Twitter": tmarker,
                                "OSM": osmmarker,
                                "Sensors": sensormarker,
                                },
                    };


L.control.groupedLayers(baseMaps, groupedOverlays, {collapsed: false}).addTo(map);

//Add event of show/hide frames in the rain sensor button
jQuery(document).ready(function(){
        jQuery('#hideshow').on('click', function(event) {        
             jQuery('#sensorbox').toggle('show');
        });
    });