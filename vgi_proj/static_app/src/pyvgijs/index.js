// Clear local storage 
localStorage.clear();

//Set basemap view and Create tile Layers for the basemaps
var map = L.map('mapid').setView([40.207190, -8.429730], 4);
var osm = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {maxZoom: 19,attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'}).addTo(map);
var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {maxZoom:19, attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'});

//Create sensor marker icon
var sicon = L.icon({
            iconUrl: 'https://png.icons8.com/color/1600/sensor',
                    iconSize: [30,30],
                    iconAnchor: [12.5,30],
                    popupAnchor: [0,-30]
                    });

//Create sensor marker
var sensormarker = L.marker([40.185606, -8.414708],{icon: sicon}).addTo(map);
sensormarker.bindPopup("<b style='color:DeepSkyBlue;'>Rain Sensor</b>"+"<br>"+"<b>Civil Engineering Department of University of Coimbra</b>")

// Add the search field (cities, countries and so on) for the basemaps
map.addControl( new L.Control.Search({
    url: 'http://nominatim.openstreetmap.org/search?format=json&accept-language=en&q={s}',
    jsonpParam: 'json_callback',
    propertyName: 'display_name',
    propertyLoc: ['lat','lon'],
    marker: false,
    minLength: 2,
    zoom: 12
        }) );

// Add the buttons for the basemap control and the draw options
drawnItems = L.featureGroup().addTo(map);
L.control.layers({
    'osm': osm.addTo(map),
    "sattelite": Esri_WorldImagery
},
{ 'drawlayer': drawnItems, 'sensormarker': sensormarker }, { position: 'topleft', collapsed: false }).addTo(map);


map.addControl(new L.Control.Draw({
    edit: {
        featureGroup: drawnItems, sensormarker,
        poly: {
            allowIntersection: false
        }
    },
    draw: {
        polyline : false,
        rectangle : false,
        circle : false,
        polygon: false,
        marker: {
        }
    }
}));

map.on(L.Draw.Event.CREATED, function (event) {
    var layer = event.layer;        
    drawnItems.addLayer(layer);
    //Fill the form with the drawn marker coordinates
    var lat = layer._latlng['lat'];
    var lng = layer._latlng['lng'];
    document.getElementById('myForm').elements[21].value = lat.toString();
    document.getElementById('myForm').elements[22].value = lng.toString();               
});


//Hide the osm values forms
$('#amenity, #barrier, #boundary, #building, #highway, #historic, #landuse, #leisure, #manmade, #natural, #office, #place, #publictransport, #railway, #shop, #tourism, #waterway').hide();

//Store the value of the osm key in local storage to be used in the layers.html page
$('#key').change(function () {
    localStorage["osmkey"] = document.getElementById('key').value;
});

//Reveal the osm value form according to the osm feature selected 
$('#key').change(function () {
    var selected = $('#key option:selected').text();
    $('#amenity').toggle(selected == "amenity");
});

$('#key').change(function () {
    var selected = $('#key option:selected').text();
    $('#barrier').toggle(selected == "barrier");
});

$('#key').change(function () {
    var selected = $('#key option:selected').text();
    $('#boundary').toggle(selected == "boundary");
});

$('#key').change(function () {
    var selected = $('#key option:selected').text();
    $('#building').toggle(selected == "building");
});

$('#key').change(function () {
    var selected = $('#key option:selected').text();
    $('#highway').toggle(selected == "highway");
});

$('#key').change(function () {
    var selected = $('#key option:selected').text();
    $('#historic').toggle(selected == "historic");
});

$('#key').change(function () {
    var selected = $('#key option:selected').text();
    $('#landuse').toggle(selected == "landuse");
});

$('#key').change(function () {
    var selected = $('#key option:selected').text();
    $('#leisure').toggle(selected == "leisure");
});

$('#key').change(function () {
    var selected = $('#key option:selected').text();
    $('#manmade').toggle(selected == "man_made");
});

$('#key').change(function () {
    var selected = $('#key option:selected').text();
    $('#natural').toggle(selected == "natural");
});

$('#key').change(function () {
    var selected = $('#key option:selected').text();
    $('#office').toggle(selected == "office");
});

$('#key').change(function () {
    var selected = $('#key option:selected').text();
    $('#place').toggle(selected == "place");
});

$('#key').change(function () {
    var selected = $('#key option:selected').text();
    $('#publictransport').toggle(selected == "public_transport");
});

$('#key').change(function () {
    var selected = $('#key option:selected').text();
    $('#railway').toggle(selected == "railway");
});

$('#key').change(function () {
    var selected = $('#key option:selected').text();
    $('#shop').toggle(selected == "shop");
});

$('#key').change(function () {
    var selected = $('#key option:selected').text();
    $('#tourism').toggle(selected == "tourism");
});

$('#key').change(function () {
    var selected = $('#key option:selected').text();
    $('#waterway').toggle(selected == "waterway");
});

//Add a scrollbar (perfect scrollbar) to the form
var container = document.getElementById('text');
Ps.initialize(container);

//Add event of show/hide frames in the rain sensor and how to use buttons
jQuery(document).ready(function(){
        jQuery('#hideshow').on('click', function(event) {        
             jQuery('#sensorbox').toggle('show');
        });
    });
    
jQuery(document).ready(function(){
        jQuery('#howtobutton').on('click', function(event) {        
             jQuery('#howtouse').toggle('show');
        });
    });