from __future__ import unicode_literals

from django.apps import AppConfig


class StaticAppConfig(AppConfig):
    name = 'static_app'
