from __future__ import unicode_literals

from django.apps import AppConfig


class PyvgiAppConfig(AppConfig):
    name = 'pyvgi_app'
