import os
from django.contrib.gis.utils import LayerMapping
from .models import AmenityPt, BarrierLine, BarrierPt, BoundaryLine, BuildingLine, BuildingPt, HighwayLine, HighwayPt, HistoricLine, HistoricPt, LanduseLine, LeisureLine, LeisurePt, ManMadeLine, ManMadePt, NaturalLine, NaturalPt, Office, Place, PublicTransportLine, PublicTransportPt, RailwayLine, RailwayPt, ShopLine, ShopPt, TourismLine, TourismPt, WaterwayLine
from pyvgi.database.deletedata import delete


def map_osm():
    '''
    This function is used to update OSM data in the database from the
    shapefiles downloaded from OSM
    '''
    #delete tables data
    delete('amenitypt')
    delete('barrierline')
    delete('barrierpt')
    delete('boundaryline')
    delete('buildingline')
    delete('buildingpt')
    delete('highwayline')
    delete('highwaypt')
    delete('historicline')
    delete('historicpt')
    delete('landuseline')
    delete('leisureline')
    delete('leisurept')
    delete('manmadeline')
    delete('manmadept')
    delete('naturalline')
    delete('naturalpt')
    delete('office')
    delete('place')
    delete('publictransportline')
    delete('publictransportpt')
    delete('railwayline')
    delete('railwaypt')
    delete('shopline')
    delete('shoppt')
    delete('tourismline')
    delete('tourismpt')
    delete('waterwayline')
    
    #Add the new data to tables    
    amenitypt_mapping = {
        'amenity' : 'amenity',
        'name' : 'name',
        'geom' : 'MULTIPOINT',
    }
    
    amp_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/amenityPoint.shp'))
    verbose=True
    amp = LayerMapping(AmenityPt, amp_ogr, amenitypt_mapping, transform=False, encoding='iso-8859-1')
    amp.save(strict=True, verbose=verbose)
    
    barrierline_mapping = {
        'barrier' : 'barrier',
        'name' : 'name',
        'geom' : 'MULTILINESTRING',
    }
    
    barrl_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/barrierLine.shp'))
    verbose=True
    barrl = LayerMapping(BarrierLine, barrl_ogr, barrierline_mapping, transform=False, encoding='iso-8859-1')
    barrl.save(strict=True, verbose=verbose)
    
    barrierpt_mapping = {
        'barrier' : 'barrier',
        'name' : 'name',
        'geom' : 'MULTIPOINT',
    }
    
    barrpt_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/barrierPoint.shp'))
    verbose=True
    barrpt = LayerMapping(BarrierPt, barrpt_ogr, barrierpt_mapping, transform=False, encoding='iso-8859-1')
    barrpt.save(strict=True, verbose=verbose)
    
    boundaryline_mapping = {
        'boundary' : 'boundary',
        'border_typ' : 'border_typ',
        'geom' : 'MULTILINESTRING',
    }
    
    boundl_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/boundaryLine.shp'))
    verbose=True
    boundl = LayerMapping(BoundaryLine, boundl_ogr, boundaryline_mapping, transform=False, encoding='iso-8859-1')
    boundl.save(strict=True, verbose=verbose)
    
    buildingline_mapping = {
        'building' : 'building',
        'name' : 'name',
        'geom' : 'MULTILINESTRING',
    }
    
    buildl_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/buildingLine.shp'))
    verbose=True
    buildl = LayerMapping(BuildingLine, buildl_ogr, buildingline_mapping, transform=False, encoding='iso-8859-1')
    buildl.save(strict=True, verbose=verbose)
    
    buildingpt_mapping = {
        'building' : 'building',
        'name' : 'name',
        'geom' : 'MULTIPOINT',
    }
    
    buildpt_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/buildingPoint.shp'))
    verbose=True
    buildpt = LayerMapping(BuildingPt, buildpt_ogr, buildingpt_mapping, transform=False, encoding='iso-8859-1')
    buildpt.save(strict=True, verbose=verbose)
    
    highwayline_mapping = {
        'highway' : 'highway',
        'name' : 'name',
        'geom' : 'MULTILINESTRING',
    }
    
    highl_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/highwayLine.shp'))
    verbose=True
    highl = LayerMapping(HighwayLine, highl_ogr, highwayline_mapping, transform=False, encoding='iso-8859-1')
    highl.save(strict=True, verbose=verbose)
    
    highwaypt_mapping = {
        'highway' : 'highway',
        'name' : 'name',
        'geom' : 'MULTIPOINT',
    }
    
    highpt_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/highwayPoint.shp'))
    verbose=True
    highpt = LayerMapping(HighwayPt, highpt_ogr, highwaypt_mapping, transform=False, encoding='iso-8859-1')
    highpt.save(strict=True, verbose=verbose)
    
    historicline_mapping = {
        'historic' : 'historic',
        'name' : 'name',
        'geom' : 'MULTILINESTRING',
    }
    
    histl_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/historicLine.shp'))
    verbose=True
    histl = LayerMapping(HistoricLine, histl_ogr, historicline_mapping, transform=False, encoding='iso-8859-1')
    histl.save(strict=True, verbose=verbose)
    
    historicpt_mapping = {
        'historic' : 'historic',
        'name' : 'name',
        'geom' : 'MULTIPOINT',
    }
    
    histpt_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/historicPoint.shp'))
    verbose=True
    histpt = LayerMapping(HistoricPt, histpt_ogr, historicpt_mapping, transform=False, encoding='iso-8859-1')
    histpt.save(strict=True, verbose=verbose)
    
    landuseline_mapping = {
        'landuse' : 'landuse',
        'name' : 'name',
        'geom' : 'MULTILINESTRING',
    }
    
    landl_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/landuseLine.shp'))
    verbose=True
    landl = LayerMapping(LanduseLine, landl_ogr, landuseline_mapping, transform=False, encoding='iso-8859-1')
    landl.save(strict=True, verbose=verbose)
    
    leisureline_mapping = {
        'leisure' : 'leisure',
        'name' : 'name',
        'geom' : 'MULTILINESTRING',
    }
    
    leil_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/leisureLine.shp'))
    verbose=True
    leil = LayerMapping(LeisureLine, leil_ogr, leisureline_mapping, transform=False, encoding='iso-8859-1')
    leil.save(strict=True, verbose=verbose)
    
    leisurept_mapping = {
        'leisure' : 'leisure',
        'name' : 'name',
        'geom' : 'MULTIPOINT',
    }
    
    leipt_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/leisurePoint.shp'))
    verbose=True
    leipt = LayerMapping(LeisurePt, leipt_ogr, leisurept_mapping, transform=False, encoding='iso-8859-1')
    leipt.save(strict=True, verbose=verbose)
    
    manmadeline_mapping = {
        'man_made' : 'man_made',
        'name' : 'name',
        'geom' : 'MULTILINESTRING',
    }
    
    mml_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/man_madeLine.shp'))
    verbose=True
    mml = LayerMapping(ManMadeLine, mml_ogr, manmadeline_mapping, transform=False, encoding='iso-8859-1')
    mml.save(strict=True, verbose=verbose)
    
    manmadept_mapping = {
        'man_made' : 'man_made',
        'name' : 'name',
        'geom' : 'MULTIPOINT',
    }
    
    mmpt_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/man_madePoint.shp'))
    verbose=True
    mmpt = LayerMapping(ManMadePt, mmpt_ogr, manmadept_mapping, transform=False, encoding='iso-8859-1')
    mmpt.save(strict=True, verbose=verbose)
    
    naturalline_mapping = {
        'natural' : 'natural',
        'water' : 'water',
        'geom' : 'MULTILINESTRING',
    }
    
    natl_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/naturalLine.shp'))
    verbose=True
    natl = LayerMapping(NaturalLine, natl_ogr, naturalline_mapping, transform=False, encoding='iso-8859-1')
    natl.save(strict=True, verbose=verbose)
    
    naturalpt_mapping = {
        'natural' : 'natural',
        'water' : 'water',
        'geom' : 'MULTIPOINT',
    }
    
    natpt_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/naturalPoint.shp'))
    verbose=True
    natpt = LayerMapping(NaturalPt, natpt_ogr, naturalpt_mapping, transform=False, encoding='iso-8859-1')
    natpt.save(strict=True, verbose=verbose)
    
    office_mapping = {
        'office' : 'office',
        'name' : 'name',
        'geom' : 'MULTIPOINT',
    }
    
    of_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/office.shp'))
    verbose=True
    of = LayerMapping(Office, of_ogr, office_mapping, transform=False, encoding='iso-8859-1')
    of.save(strict=True, verbose=verbose)
    
    place_mapping = {
        'place' : 'place',
        'name' : 'name',
        'geom' : 'MULTIPOINT',
    }
    
    pl_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/place.shp'))
    verbose=True
    pl = LayerMapping(Place, pl_ogr, place_mapping, transform=False, encoding='iso-8859-1')
    pl.save(strict=True, verbose=verbose)
    
    publictransportline_mapping = {
        'public_tra' : 'public_tra',
        'name' : 'name',
        'geom' : 'MULTILINESTRING',
    }
    
    ptl_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/public_transportLine.shp'))
    verbose=True
    ptl = LayerMapping(PublicTransportLine, ptl_ogr, publictransportline_mapping, transform=False, encoding='iso-8859-1')
    ptl.save(strict=True, verbose=verbose)
    
    publictransportpt_mapping = {
        'public_tra' : 'public_tra',
        'name' : 'name',
        'geom' : 'MULTIPOINT',
    }
    
    ptpt_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/public_transportPoint.shp'))
    verbose=True
    ptpt = LayerMapping(PublicTransportPt, ptpt_ogr, publictransportpt_mapping, transform=False, encoding='iso-8859-1')
    ptpt.save(strict=True, verbose=verbose)
    
    railwayline_mapping = {
        'railway' : 'railway',
        'name' : 'name',
        'geom' : 'MULTILINESTRING',
    }
    
    rwl_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/railwayLine.shp'))
    verbose=True
    rwl = LayerMapping(RailwayLine, rwl_ogr, railwayline_mapping, transform=False, encoding='iso-8859-1')
    rwl.save(strict=True, verbose=verbose)
    
    railwaypt_mapping = {
        'railway' : 'railway',
        'name' : 'name',
        'geom' : 'MULTIPOINT',
    }
    
    rwpt_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/railwayPoint.shp'))
    verbose=True
    rwpt = LayerMapping(RailwayPt, rwpt_ogr, railwaypt_mapping, transform=False, encoding='iso-8859-1')
    rwpt.save(strict=True, verbose=verbose)
    
    shopline_mapping = {
        'shop' : 'shop',
        'name' : 'name',
        'geom' : 'MULTILINESTRING',
    }
    
    sl_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/shopLine.shp'))
    verbose=True
    sl = LayerMapping(ShopLine, sl_ogr, shopline_mapping, transform=False, encoding='iso-8859-1')
    sl.save(strict=True, verbose=verbose)
    
    shoppt_mapping = {
        'shop' : 'shop',
        'name' : 'name',
        'geom' : 'MULTIPOINT',
    }
    
    spt_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/shopPoint.shp'))
    verbose=True
    spt = LayerMapping(ShopPt, spt_ogr, shoppt_mapping, transform=False, encoding='iso-8859-1')
    spt.save(strict=True, verbose=verbose)
    
    tourismline_mapping = {
        'tourism' : 'tourism',
        'name' : 'name',
        'geom' : 'MULTILINESTRING',
    }
    
    tl_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/tourismLine.shp'))
    verbose=True
    tl = LayerMapping(TourismLine, tl_ogr, tourismline_mapping, transform=False, encoding='iso-8859-1')
    tl.save(strict=True, verbose=verbose)
    
    tourismpt_mapping = {
        'tourism' : 'tourism',
        'name' : 'name',
        'geom' : 'MULTIPOINT',
    }
    
    tpt_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/tourismPoint.shp'))
    verbose=True
    tpt = LayerMapping(TourismPt, tpt_ogr, tourismpt_mapping, transform=False, encoding='iso-8859-1')
    tpt.save(strict=True, verbose=verbose)
    
    waterwayline_mapping = {
        'waterway' : 'waterway',
        'name' : 'name',
        'geom' : 'MULTILINESTRING',
    }
    
    wwl_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/osm/waterwayLine.shp'))
    verbose=True
    wwl = LayerMapping(WaterwayLine, wwl_ogr, waterwayline_mapping, transform=False, encoding='iso-8859-1')
    wwl.save(strict=True, verbose=verbose)
