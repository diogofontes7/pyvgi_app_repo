import os
from vgi_proj.settings import *

#leaflet settings configurations
LEAFLET_CONFIG = {
    'DEFAULT_CENTER': (40.207668, -8.429613),
    'DEFAULT_ZOOM': 15,
    'MAX_ZOOM': 19,
    'SCALE': 'both',
}

#Directory of data files
DATA_DIR = os.path.join(BASE_DIR, 'pyvgi_app', 'data')