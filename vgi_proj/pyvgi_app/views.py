import os
import shutil
import json
import random

from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.core.serializers import serialize
from django.contrib.gis.utils import LayerMapping

from .models import AmenityPt, BarrierLine, BarrierPt, BoundaryLine, BuildingLine, BuildingPt, HighwayLine, HighwayPt, HistoricLine, HistoricPt, LanduseLine, LeisureLine, LeisurePt, ManMadeLine, ManMadePt, NaturalLine, NaturalPt, Office, Place, PublicTransportLine, PublicTransportPt, RailwayLine, RailwayPt, ShopLine, ShopPt, TourismLine, TourismPt, WaterwayLine
from .forms import UserInput
from .lsettings import *

from pyvgi.flickr.flickrAPI import flickr_map_data
from pyvgi.twitter.twitterAPI import twitter_map_data
from pyvgi.pyobj.t import generate_random_string
from pyvgi.database.deletedata import delete
from pyvgi.vector.create import ogr_format_conversion


def index(request):
    '''
    View for the index page with the form created
    '''
    form = UserInput()
    context = {
        'title':'User Input',
        'form': form,
    }
    return render(request, 'pyvgi_app/index.html', context)


def layers(request):
    '''
    View for the layers page with the 
    tools to querie data from flickr and twitter
    '''
    
    #if statements to check the form values    
    if request.POST.get('lat') == "":
        lat = 40.20719000000
    else: 
        lat = float(request.POST.get('lat'))
    
    if request.POST.get('lon') == "":
        lon = -8.42973000000
    else:
        lon = float(request.POST.get('lon'))
    
    if request.POST.get('keyw') == "":
        kw = ""
    else:
        kw = request.POST.get('keyw')
    
    if request.POST.get('rad') == "":
        rd = 2.5
    else:
        rd = float(request.POST.get('rad'))
    
    if request.POST.get('perp') == "":
        pp = 100
    else:
        pp = int(request.POST.get('perp'))      
    
    #Create a random string with numbers and characters to identify
    # each user and create a folder for each one, checking is there is 
    # a folder with the same name
    id_user = generate_random_string(random.randint(10,20))
    folder = os.path.abspath(os.path.join(os.path.dirname(__file__),'data/user'+id_user))
    if os.path.exists(folder):
        shutil.rmtree(folder)    
    os.mkdir(folder)
    
    
    #Map data from flickr and twitter
    flickr_mapping = {
        'title' : 'Title',
        'takendate' : 'TakenDate',
        'uploaddate' : 'UploadDate',
        'url' : 'URL',
        'geom' : 'MULTIPOINT',
    }    
    
    flickr_ogr = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/user'+id_user+'/flickr.shp'))
    flickr_json = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                              'data/user'+id_user+'/flickr.json'))    
    
    twitter_mapping = {
        'url' : 'URL',
        'dscription' : 'Dscription',
        'uploaddate' : 'UploadDate',
        'geom' : 'MULTIPOINT',
    }
  
    twitter_ogr = os.path.join(os.path.dirname(os.path.abspath(__file__)), 
                               'data/user'+id_user+'/twitter.shp')
    twitter_json = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                'data/user'+id_user+'/twitter.json')    
    
    
    #Check if flickr or twitter were selected in the form 
    # call the function to querie their data and add tranform shp into json file
    if request.POST.get('fl') == 'on' and lat != 40.20719000000:
        flickr_map_data(kw, lat, lon, rd, pp, flickr_ogr)
        ogr_format_conversion(flickr_ogr, flickr_json)
    else:
        pass
        
    if request.POST.get('tw') == 'on' and lat != 40.20719000000:    
        twitter_map_data(kw, lat, lon, rd, pp, twitter_ogr)
        ogr_format_conversion(twitter_ogr, twitter_json)
    else: 
        pass
    
    #create a global variable to store the selected osm value in the form   
    global value 
    value = (request.POST['a_value'], request.POST['ba_value'], 
             request.POST['bo_value'], request.POST['bu_value'], 
             request.POST['hig_value'], request.POST['his_value'], 
             request.POST['lan_value'], request.POST['lei_value'], 
             request.POST['man_value'], request.POST['nat_value'], 
             request.POST['o_value'], request.POST['pl_value'], 
             request.POST['pt_value'], request.POST['r_value'], 
             request.POST['s_value'], request.POST['t_value'], 
             request.POST['w_value'])
    
    #Get json file names
    file_name_f = os.path.basename(os.path.splitext(flickr_json)[0])
    file_name_t = os.path.basename(os.path.splitext(twitter_json)[0])
    
    return render(request, 'pyvgi_app/layers.html', {'lat':lat, 'lng':lon, 
                                                     'id_user':id_user, 
                                                     'file_name_f':file_name_f, 
                                                     'file_name_t':file_name_t })


def to_geojson(request, r, f):
    '''
    Return geodata as a JSON object
    
    Used for files in the data folder
    
    Parameters:
    r = name of random folder
    f = filename.json
    '''
    
    json_file = os.path.join(
        DATA_DIR, 'user' + str(r), str(f) + '.json'
    )
    
    with open(json_file, mode='r') as f:
        data = json.load(f)
    
    return JsonResponse(data)


#Create the osm datasets checking for the value of the osm value form
def amenitypt_datasets(request,):
    if value[0] == " ":
        amenity = serialize('geojson', AmenityPt.objects.all())
    else:
        amenity = serialize('geojson', AmenityPt.objects.filter(amenity=value[0]))
    return HttpResponse(amenity, content_type='json')

def barrierline_datasets(request):
    if value[1] == " ":
        barrier = serialize('geojson', BarrierLine.objects.all())
    else:
        barrier = serialize('geojson', BarrierLine.objects.filter(barrier=value[1]))
    return HttpResponse(barrier, content_type='json')

def barrierpt_datasets(request):
    if value[1] == " ":
        barrier = serialize('geojson', BarrierPt.objects.all())
    else:
        barrier = serialize('geojson', BarrierPt.objects.filter(barrier=value[1]))
    return HttpResponse(barrier, content_type='json')

def boundaryline_datasets(request):
    if value[2] == " ":
        boundary = serialize('geojson', BoundaryLine.objects.all())
    else: 
        boundary = serialize('geojson', BoundaryLine.objects.filter(boundary=value[2]))
    return HttpResponse(boundary, content_type='json')

def buildingline_datasets(request):
    if value[3] == " ":
        building = serialize('geojson', BuildingLine.objects.all())
    else:
        building = serialize('geojson', BuildingLine.objects.filter(building=value[3]))
    return HttpResponse(building, content_type='json')

def buildingpt_datasets(request):
    if value[3] == " ":
        building = serialize('geojson', BuildingPt.objects.all())
    else:
        building = serialize('geojson', BuildingPt.objects.filter(building=value[3]))    
    return HttpResponse(building, content_type='json')

def highwayline_datasets(request):
    if value[4] == " ":
        highway = serialize('geojson', HighwayLine.objects.all())
    else:
        highway = serialize('geojson', HighwayLine.objects.filter(highway=value[4]))
    return HttpResponse(highway, content_type='json')

def highwaypt_datasets(request):
    if value[4] == " ":
        highway = serialize('geojson', HighwayPt.objects.all())
    else:
        highway = serialize('geojson', HighwayPt.objects.filter(highway=value[4]))
    return HttpResponse(highway, content_type='json')

def historicline_datasets(request):
    if value[5] == " ":
        historic = serialize('geojson', HistoricLine.objects.all())
    else:
        historic = serialize('geojson', HistoricLine.objects.filter(historic=value[5]))
    return HttpResponse(historic, content_type='json')

def historicpt_datasets(request):
    if value[5] == " ":
        historic = serialize('geojson', HistoricPt.objects.all())
    else:
        historic = serialize('geojson', HistoricPt.objects.filter(historic=value[5]))
    return HttpResponse(historic, content_type='json')

def landuseline_datasets(request):
    if value[6] == " ":
        landuse = serialize('geojson', LanduseLine.objects.all())
    else:
        landuse = serialize('geojson', LanduseLine.objects.filter(landuse=value[6]))
    return HttpResponse(landuse, content_type='json')

def leisureline_datasets(request):
    if value[7] == " ":
        leisure = serialize('geojson', LeisureLine.objects.all())
    else:
        leisure = serialize('geojson', LeisureLine.objects.filter(leisure=value[7]))
    return HttpResponse(leisure, content_type='json')

def leisurept_datasets(request):
    if value[7] == " ":
        leisure = serialize('geojson', LeisurePt.objects.all())
    else:
        leisure = serialize('geojson', LeisurePt.objects.filter(leisure=value[7]))
    return HttpResponse(leisure, content_type='json')

def manmadeline_datasets(request):
    if value[8] == " ":
        man_made = serialize('geojson', ManMadeLine.objects.all())
    else:
        man_made = serialize('geojson', ManMadeLine.objects.filter(man_made=value[8]))
    return HttpResponse(man_made, content_type='json')

def manmadept_datasets(request):
    if value[8] == " ":
        man_made = serialize('geojson', ManMadePt.objects.all())
    else:
        man_made = serialize('geojson', ManMadePt.objects.filter(man_made=value[8]))
    return HttpResponse(man_made, content_type='json')

def naturalline_datasets(request):
    if value[9] == " ":
        natural = serialize('geojson', NaturalLine.objects.all())
    else:
        natural = serialize('geojson', NaturalLine.objects.filter(natural=value[9]))
    return HttpResponse(natural, content_type='json')

def naturalpt_datasets(request):
    if value[9] == " ":
        natural = serialize('geojson', NaturalPt.objects.all())
    else:
        natural = serialize('geojson', NaturalPt.objects.filter(natural=value[9]))
    return HttpResponse(natural, content_type='json')

def office_datasets(request):
    if value[10] == " ":
        office = serialize('geojson', Office.objects.all())
    else:
        office = serialize('geojson', Office.objects.filter(office=value[10]))
    return HttpResponse(office, content_type='json')

def place_datasets(request):
    if value[11] == " ":
        place = serialize('geojson', Place.objects.all())
    else:
        place = serialize('geojson', Place.objects.filter(place=value[11]))
    return HttpResponse(place, content_type='json')

def publictransportline_datasets(request):
    if value[12] == " ":
        public_transport = serialize('geojson', PublicTransportLine.objects.all())
    else:
        public_transport = serialize('geojson', PublicTransportLine.objects.filter(public_tra=value[12]))
    return HttpResponse(public_transport, content_type='json')

def publictransportpt_datasets(request):
    if value[12] == " ":
        public_transport = serialize('geojson', PublicTransportPt.objects.all())
    else:
        public_transport = serialize('geojson', PublicTransportPt.objects.filter(public_tra=value[12]))    
    return HttpResponse(public_transport, content_type='json')

def railwayline_datasets(request):
    if value[13] == " ":
        railway = serialize('geojson', RailwayLine.objects.all())
    else:
        railway = serialize('geojson', RailwayLine.objects.filter(railway=value[13]))
    return HttpResponse(railway, content_type='json')

def railwaypt_datasets(request):
    if value[13] == " ":
        railway = serialize('geojson', RailwayPt.objects.all())
    else:
        railway = serialize('geojson', RailwayPt.objects.filter(railway=value[13]))    
    return HttpResponse(railway, content_type='json')

def shopline_datasets(request):
    if value[14] == " ":
        shop = serialize('geojson', ShopLine.objects.all())
    else:
        shop = serialize('geojson', ShopLine.objects.filter(shop=value[14]))
    return HttpResponse(shop, content_type='json')

def shoppt_datasets(request):
    if value[14] == " ":
        shop = serialize('geojson', ShopPt.objects.all())
    else:
        shop = serialize('geojson', ShopPt.objects.filter(shop=value[14]))
    return HttpResponse(shop, content_type='json')

def tourismline_datasets(request):
    if value[15] == " ":
        tourism = serialize('geojson', TourismLine.objects.all())
    else:
        tourism = serialize('geojson', TourismLine.objects.filter(tourism=value[15]))
    return HttpResponse(tourism, content_type='json')

def tourismpt_datasets(request):
    if value[15] == " ":
        tourism = serialize('geojson', TourismPt.objects.all())
    else:
        tourism = serialize('geojson', TourismPt.objects.filter(tourism=value[15]))
    return HttpResponse(tourism, content_type='json')

def waterwayline_datasets(request):
    if value[15] == " ":
        waterway = serialize('geojson', WaterwayLine.objects.all())
    else:
        waterway = serialize('geojson', WaterwayLine.objects.filter(waterway=value[16]))
    return HttpResponse(waterway, content_type='json')