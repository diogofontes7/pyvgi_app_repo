from django.contrib import admin
from .models import AmenityPt, BarrierLine, BarrierPt, BoundaryLine, BuildingLine, BuildingPt, HighwayLine, HighwayPt, HistoricLine, HistoricPt, LanduseLine, LeisureLine, LeisurePt, ManMadeLine, ManMadePt, NaturalLine, NaturalPt, Office, Place, PublicTransportLine, PublicTransportPt, RailwayLine, RailwayPt, ShopLine, ShopPt, TourismLine, TourismPt, WaterwayLine
from leaflet.admin import LeafletGeoAdmin


#Register the models
class AmenityPtAdmin(LeafletGeoAdmin):
    list_display = ('amenity', 'name')
    
class BarrierLineAdmin(LeafletGeoAdmin):
    list_display = ('barrier', 'name')

class BarrierPtAdmin(LeafletGeoAdmin):
    list_display = ('barrier', 'name')

class BoundaryLineAdmin(LeafletGeoAdmin):
    list_display = ('boundary', 'border_typ')

class BuildingLineAdmin(LeafletGeoAdmin):
    list_display = ('building', 'name')

class BuildingPtAdmin(LeafletGeoAdmin):
    list_display = ('building', 'name')

class HighwayLineAdmin(LeafletGeoAdmin):
    list_display = ('highway', 'name')

class HighwayPtAdmin(LeafletGeoAdmin):
    list_display = ('highway', 'name')

class HistoricLineAdmin(LeafletGeoAdmin):
    list_display = ('historic', 'name')

class HistoricPtAdmin(LeafletGeoAdmin):
    list_display = ('historic', 'name')

class LanduseLineAdmin(LeafletGeoAdmin):
    list_display = ('landuse', 'name')
    
class LeisureLineAdmin(LeafletGeoAdmin):
    list_display = ('leisure', 'name')

class LeisurePtAdmin(LeafletGeoAdmin):
    list_display = ('leisure', 'name')

class ManMadeLineAdmin(LeafletGeoAdmin):
    list_display = ('man_made', 'name')

class ManMadePtAdmin(LeafletGeoAdmin):
    list_display = ('man_made', 'name')

class NaturalLineAdmin(LeafletGeoAdmin):
    list_display = ('natural', 'water')

class NaturalPtAdmin(LeafletGeoAdmin):
    list_display = ('natural', 'water')

class OfficeAdmin(LeafletGeoAdmin):
    list_display = ('office', 'name')

class PlaceAdmin(LeafletGeoAdmin):
    list_display = ('place', 'name')

class PublicTransportLineAdmin(LeafletGeoAdmin):
    list_display = ('public_tra', 'name')

class PublicTransportPtAdmin(LeafletGeoAdmin):
    list_display = ('public_tra', 'name')

class RailwayLineAdmin(LeafletGeoAdmin):
    list_display = ('railway', 'name')

class RailwayPtAdmin(LeafletGeoAdmin):
    list_display = ('railway', 'name')

class ShopLineAdmin(LeafletGeoAdmin):
    list_display = ('shop', 'name')

class ShopPtAdmin(LeafletGeoAdmin):
    list_display = ('shop', 'name')

class TourismLineAdmin(LeafletGeoAdmin):
    list_display = ('tourism', 'name')

class TourismPtAdmin(LeafletGeoAdmin):
    list_display = ('tourism', 'name')

class WaterwayLineAdmin(LeafletGeoAdmin):
    list_display = ('waterway', 'name')

admin.site.register(AmenityPt, AmenityPtAdmin)
admin.site.register(BarrierLine, BarrierLineAdmin)
admin.site.register(BarrierPt, BarrierPtAdmin)
admin.site.register(BoundaryLine, BoundaryLineAdmin)
admin.site.register(BuildingLine, BuildingLineAdmin)
admin.site.register(BuildingPt, BuildingPtAdmin)
admin.site.register(HighwayLine, HighwayLineAdmin)
admin.site.register(HighwayPt, HighwayPtAdmin)
admin.site.register(HistoricLine, HistoricLineAdmin)
admin.site.register(HistoricPt, HistoricPtAdmin)
admin.site.register(LanduseLine, LanduseLineAdmin)
admin.site.register(LeisureLine, LeisureLineAdmin)
admin.site.register(LeisurePt, LeisurePtAdmin)
admin.site.register(ManMadeLine, ManMadeLineAdmin)
admin.site.register(ManMadePt, ManMadePtAdmin)
admin.site.register(NaturalLine, NaturalLineAdmin)
admin.site.register(NaturalPt, NaturalPtAdmin)
admin.site.register(Office, OfficeAdmin)
admin.site.register(Place, PlaceAdmin)
admin.site.register(PublicTransportLine, PublicTransportLineAdmin)
admin.site.register(PublicTransportPt, PublicTransportPtAdmin)
admin.site.register(RailwayLine, RailwayLineAdmin)
admin.site.register(RailwayPt, RailwayPtAdmin)
admin.site.register(ShopLine, ShopLineAdmin)
admin.site.register(ShopPt, ShopPtAdmin)
admin.site.register(TourismLine, TourismLineAdmin)
admin.site.register(TourismPt, TourismPtAdmin)
admin.site.register(WaterwayLine, WaterwayLineAdmin)