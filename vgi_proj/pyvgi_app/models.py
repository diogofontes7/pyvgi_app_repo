from __future__ import unicode_literals
from django.db import models
from django.contrib.gis.db import models 

#Models for OSM data    
class AmenityPt(models.Model):
    amenity = models.CharField(max_length=17)
    name = models.CharField(max_length=58)
    geom = models.MultiPointField(srid=4326)
    
    def __unicode__(self):
        return self.amenity
    
class BarrierLine(models.Model):
    barrier = models.CharField(max_length=15)
    name = models.CharField(max_length=13)
    geom = models.MultiLineStringField(srid=4326)
    
    def __unicode__(self):
        return self.barrier    

class BarrierPt(models.Model):
    barrier = models.CharField(max_length=15)
    name = models.CharField(max_length=13)
    geom = models.MultiPointField(srid=4326)
    
    def __unicode__(self):
        return self.barrier    

class BoundaryLine(models.Model):
    boundary = models.CharField(max_length=15)
    border_typ = models.CharField(max_length=13)
    geom = models.MultiLineStringField(srid=4326)
    
    def __unicode__(self):
        return self.boundary    

class BuildingLine(models.Model):
    building = models.CharField(max_length=15)
    name = models.CharField(max_length=100)
    geom = models.MultiLineStringField(srid=4326)
    
    def __unicode__(self):
        return self.building    

class BuildingPt(models.Model):
    building = models.CharField(max_length=15)
    name = models.CharField(max_length=100)
    geom = models.MultiPointField(srid=4326)
    
    def __unicode__(self):
        return self.building    

class HighwayLine(models.Model):
    highway = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    geom = models.MultiLineStringField(srid=4326)
    
    def __unicode__(self):
        return self.highway    

class HighwayPt(models.Model):
    highway = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    geom = models.MultiPointField(srid=4326)
    
    def __unicode__(self):
        return self.highway    

class HistoricLine(models.Model):
    historic = models.CharField(max_length=15)
    name = models.CharField(max_length=100)
    geom = models.MultiLineStringField(srid=4326)
    
    def __unicode__(self):
        return self.historic    

class HistoricPt(models.Model):
    historic = models.CharField(max_length=15)
    name = models.CharField(max_length=100)
    geom = models.MultiPointField(srid=4326)
    
    def __unicode__(self):
        return self.historic    

class LanduseLine(models.Model):
    landuse = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    geom = models.MultiLineStringField(srid=4326)
    
    def __unicode__(self):
        return self.landuse    

class LeisureLine(models.Model):
    leisure = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    geom = models.MultiLineStringField(srid=4326)
    
    def __unicode__(self):
        return self.leisure    

class LeisurePt(models.Model):
    leisure = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    geom = models.MultiPointField(srid=4326)
    
    def __unicode__(self):
        return self.leisure    

class ManMadeLine(models.Model):
    man_made = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    geom = models.MultiLineStringField(srid=4326)
    
    def __unicode__(self):
        return self.man_made    

class ManMadePt(models.Model):
    man_made = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    geom = models.MultiPointField(srid=4326)
    
    def __unicode__(self):
        return self.man_made    

class NaturalLine(models.Model):
    natural = models.CharField(max_length=50)
    water = models.CharField(max_length=100)
    geom = models.MultiLineStringField(srid=4326)
    
    def __unicode__(self):
        return self.natural    

class NaturalPt(models.Model):
    natural = models.CharField(max_length=50)
    water = models.CharField(max_length=100)
    geom = models.MultiPointField(srid=4326)
    
    def __unicode__(self):
        return self.natural    

class Office(models.Model):
    office = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    geom = models.MultiPointField(srid=4326)
    
    def __unicode__(self):
        return self.office    

class Place(models.Model):
    place = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    geom = models.MultiPointField(srid=4326)
    
    def __unicode__(self):
        return self.place    

class PublicTransportLine(models.Model):
    public_tra = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    geom = models.MultiLineStringField(srid=4326)
    
    def __unicode__(self):
        return self.public_tra    

class PublicTransportPt(models.Model):
    public_tra = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    geom = models.MultiPointField(srid=4326)
    
    def __unicode__(self):
        return self.public_tra    

class RailwayLine(models.Model):
    railway = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    geom = models.MultiLineStringField(srid=4326)
    
    def __unicode__(self):
        return self.railway    

class RailwayPt(models.Model):
    railway = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    geom = models.MultiPointField(srid=4326)
    
    def __unicode__(self):
        return self.railway    

class ShopLine(models.Model):
    shop = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    geom = models.MultiLineStringField(srid=4326)
    
    def __unicode__(self):
        return self.shop    

class ShopPt(models.Model):
    shop = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    geom = models.MultiPointField(srid=4326)
    
    def __unicode__(self):
        return self.shop    

class TourismLine(models.Model):
    tourism = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    geom = models.MultiLineStringField(srid=4326)
    
    def __unicode__(self):
        return self.tourism    

class TourismPt(models.Model):
    tourism = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    geom = models.MultiPointField(srid=4326)
    
    def __unicode__(self):
        return self.tourism    

class WaterwayLine(models.Model):
    waterway = models.CharField(max_length=50)
    name = models.CharField(max_length=100)
    geom = models.MultiLineStringField(srid=4326)
    
    def __unicode__(self):
        return self.waterway