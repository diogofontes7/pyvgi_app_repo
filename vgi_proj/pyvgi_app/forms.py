from django import forms

class UserInput(forms.Form):
    '''
    This class creates the form where the users will
    indicate the querie parameters for Twitter and Flickr
    '''
    tw = forms.BooleanField(label='Twitter', required=False, initial=True)
    fl = forms.BooleanField(label='Flickr', required=False, initial=True)
    lat = forms.CharField(label='Latitude', required=False)
    lon = forms.CharField(label='Longitude', required=False)
    keyw = forms.CharField(label='Keyword', required=False)
    rad = forms.CharField(label='Radius in km', required=False, initial='1')
    perp = forms.CharField(label='Number of posts for each source', required=False, initial='100')