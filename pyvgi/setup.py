# Setup for pyvgi

from setuptools import setup

setup(name='pyvgi',
      version='0.0',
      description='Python for VGI data requests',
      author='Diogo Fontes',
      author_email='dds.fontes@gmail.com',
      packages=['pyvgi',
                'pyvgi.flickr',
                'pyvgi.twitter',
                'pyvgi.vector',
                'pyvgi.database',
                'pyvgi.pyobj'
                ],
      )