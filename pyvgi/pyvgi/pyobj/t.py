def unicode_to_str(obj):
    '''
    Transforms a unicode type into a string type
    '''
    import unicodedata
    return unicodedata.normalize('NFKD', obj).encode('ascii', 'ignore')

def generate_random_string(char_number):
    """
    Generates a random string with numbers and characters
    """
    
    import random
    
    char = '0123456789qwertyuiopasdfghjklzxcvbnm0123456789'
    
    rnd = ''
    
    for i in range(char_number):
        rnd += random.choice(char)
    
    return rnd

