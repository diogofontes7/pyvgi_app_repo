import psycopg2
import sys


def delete(table):
    '''
    This function delete the data from a table in the indicated database
    '''
    con = None
    
    try:
    
        con = psycopg2.connect(database = 'gishub_db', host = 'localhost', user = 'postgres', password = 'admin') 
        cur = con.cursor()
        cur.execute('DELETE FROM public.pyvgi_app_%s;' %(table))          
        con.commit()
        
    
    except psycopg2.DatabaseError, e:
        
        if con:
            con.rollback()
        
        print 'Error %s' % e    
        sys.exit(1)
        
        
    finally:
        
        if con:
            con.close()