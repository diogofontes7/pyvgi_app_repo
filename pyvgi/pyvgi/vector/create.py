from osgeo import ogr
import os
import subprocess

def ogr_get_driver_name(abs_path):
    """
    This method recognize the file extension
    of the given path in order to choose 
    the vector format
    """
    drv = {
        '.shp' : 'ESRI Shapefile',
        '.gml' : 'GML',
        '.json' : 'GeoJSON',
        '.kml' : 'KML'
    }
    file_name, file_extension = os.path.splitext(abs_path)
    return drv[file_extension]

def create_fields(lyr, dic_fields):
    """
    Generate and add the fields from an input dictionary
    """
    for k in dic_fields:
        lyr.CreateField(ogr.FieldDefn(k, dic_fields[k]))

    
    
def add_geometry(outDefn, la, lo):
    """
    This method add and set the geometry
    to the vector format
    """
    #create point geometry
    point = ogr.Geometry(ogr.wkbPoint)
    point.AddPoint(lo, la)
    
    # Create the feature and set geometry
    outFeature = ogr.Feature(outDefn)
    outFeature.SetGeometry(point)
    
    return outFeature

def ogr_format_conversion(inShp, outShp):
    """
    Convert a vectorial file to another ogr driver
    """
    
    from pysage.tools_thru_api.gdal.ogr import OGR_GetDriverName
    
    outDrv = ogr_get_driver_name(outShp)
    cmd = 'ogr2ogr -f "{drv}" {out} {entrada}'.format(
        drv=outDrv, out=outShp, entrada=inShp
    )
    subprocess.call(cmd, shell=True)