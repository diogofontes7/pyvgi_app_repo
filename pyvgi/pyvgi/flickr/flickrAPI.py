
from osgeo import ogr 
import os
import datetime
from flickrapi import FlickrAPI
from pyvgi.vector.create import add_geometry
from pyvgi.vector.create import ogr_get_driver_name
from pyvgi.vector.create import create_fields
from pyvgi.pyobj.t import unicode_to_str

def flickr_map_data (
    keyword, lt, ln, rd, pp,
        out_path):
    """
    This tool connects with Flikcr API 
    in order to querie photos and some of their data
    using keywords, coordinates and a radius.
    It creates a point vector format in the directory 
    of the script with the resulted data from the querie.
    """

    #Flickr API personal parameters
    FLICKR_PUBLIC = 'b4f00205d43bfe8a4edd40f800388eb3'
    FLICKR_SECRET = 'c235398aae0743f0'

    #Request to Flickr API with the user querie parameters
    flickr = FlickrAPI(FLICKR_PUBLIC, FLICKR_SECRET, 
                       format='parsed-json', store_token=False)
    extras = 'url_l,geo,date_taken,date_upload,description'
    dataSet = flickr.photos.search(text = keyword, lat = lt, lon = ln, 
                                   radius = rd, per_page = pp, 
                                   extras = extras)
    photos = dataSet['photos']
    photo = photos['photo'] 
        
    #create vector format
    shpDriver = ogr.GetDriverByName(ogr_get_driver_name(out_path))
    
    if os.path.exists(out_path):
        shpDriver.DeleteDataSource(out_path)
    
    layer_name = os.path.splitext(os.path.basename(out_path))[0]
    layer_name = layer_name.encode('utf-8')
    outDataSource = shpDriver.CreateDataSource(out_path)
    outLayer = outDataSource.CreateLayer(
        layer_name, 
        geom_type=ogr.wkbPoint
    )
     
    #create fields
    fields = {
        'Title': ogr.OFTString,
        'Dscription': ogr.OFTString,
        'TakenDate': ogr.OFTString,
        'UploadDate': ogr.OFTString,
        'URL': ogr.OFTString
    }
    create_fields(outLayer, fields)
    featureDefn = outLayer.GetLayerDefn()

    #Cicle to extract the data and 
    #set the values to the shapefile
    for i in range(len(photo)):
        #Extract title, url, 
        #taken date, upload date and description
        a = photo[i]
        tit = unicode_to_str(a['title'])
        tit2 = tit[:149]
        desc = a['description']
        desc2 = unicode_to_str(desc['_content'])
        
        link = 'https://www.flickr.com/photos/%s/%s' %(a['owner'],a['id'])
        link = unicode_to_str(link)
        
        dateTaken = unicode_to_str(a['datetaken'])
        
        dateUpload = unicode_to_str(a['dateupload'])
        dateUpload2 = datetime.datetime.fromtimestamp(
            int(dateUpload)).strftime('%Y-%m-%d %H:%M:%S')

        #Extract coordinates 
        lat = float(a['latitude'])
        longi = float(a['longitude'])

        #Add and set geometry
        outFeature = add_geometry(featureDefn, lat, longi)

        #Set the fields
        outFeature.SetField('Title', tit2)
        #outFeature.SetField('Dscription', desc2)
        outFeature.SetField('TakenDate', dateTaken)
        outFeature.SetField('UploadDate', dateUpload2)
        outFeature.SetField('URL', link)
        outLayer.CreateFeature(outFeature)
