from osgeo import ogr
import os
import tweepy
from tweepy import OAuthHandler
from pyvgi.vector.create import add_geometry
from pyvgi.vector.create import ogr_get_driver_name
from pyvgi.vector.create import create_fields
from pyvgi.pyobj.t import unicode_to_str


def twitter_map_data(
    keyword, lt, ln, rd, pp,
        out_path):
    """
    This tool connects with Twitter API 
    in order to querie tweets data using keywords, coordinates and a radius.
    It creates a point vector format in the directory 
    of the script with the resulted data from the querie.
    """

    #Twitter personal parameters
    access_token = "3571004715-NiOnRpRrQZVGpQFvgT2B5zYB6vm3ey01ZBk9QT9"
    access_secret = "WhKkqFzshpFLzRIsS9puPTVZZgKYWhOYcf8JPcAbBFKMI"
    consumer_key = "zuDY4LEW37TCesUfObKMeMBPf"
    consumer_secret = "os60OvpWjb9TLW1ABaiZeRZy8QWcOfwknwYGLBgJOGBE5tQfrM"
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_secret)
    api = tweepy.API(auth)
    
    #create vector format
    shpDriver = ogr.GetDriverByName(ogr_get_driver_name(out_path))

    if os.path.exists(out_path):
        shpDriver.DeleteDataSource(out_path)

    layer_name = os.path.splitext(os.path.basename(out_path))[0]
    layer_name = layer_name.encode('utf-8')
    outDataSource = shpDriver.CreateDataSource(out_path)
    outLayer = outDataSource.CreateLayer(
        layer_name, 
        geom_type=ogr.wkbPoint
    )
    #create fields
    fields = {
        'Dscription': ogr.OFTString,
        'UploadDate': ogr.OFTString,
        'URL': ogr.OFTString
    }
    create_fields(outLayer, fields)
    featureDefn = outLayer.GetLayerDefn()
    
    #Request to Twitter API with the user querie parameters (max 2km radius)
    for status in tweepy.Cursor(api.search, q = keyword, 
                                geocode = str(lt) + ","
                                + str(ln) + "," + str(rd) + "km").items(pp):
        #Get the data in json format
        data = status._json

        #Extract text and upload date
        text = unicode_to_str(data['text'])
        text2= text[:117]        
        dateUpload = unicode_to_str(data['created_at'])
        
        #Extract URL
        usr = data['user']
        user = usr['screen_name']
        id_tweet = data['id']
        link = 'https://twitter.com/%s/%s/%s' %(user,'status',id_tweet)
        link = unicode_to_str(link)

        #Extract Coordinates
        geo = data['geo']
        if geo == None:
            pass
        else:
            coord = geo['coordinates']
            lati = coord[0]
            longi = coord[1]
            #Add and set geometry
            outFeature = add_geometry(featureDefn, lati, longi)
            #set values
            outFeature.SetField('Dscription', text2 )
            outFeature.SetField('UploadDate', dateUpload)
            outFeature.SetField('URL', link)
            outLayer.CreateFeature(outFeature)
